# Advent of Code 2022
## or... ADVENT-OF-COBOL-2022

Alright, so what on earth is going on here? Well I saw that advent of code was
going to happen and wanted to get in on the holiday cheer. I figured I'd do the
puzzles with my language of choice, C, and have some fun. 

...And then I thought it might be "amusing" to also see how many I could also
manage to do in COBOL. I've toyed with COBOL before but never to great extent.
I must preface however that my definition for "valid COBOL" is going to be 
fairly generous as I am using the gnuCOBOL transpiler and provided it accepts
my COBOL code as valid I'm going to call it good. Those interested may want to
turn on some of the strict flags for various COBOL standards but I'm happy as 
is. 

C solutions get posted on the day the questions are made available (barring any
emergencies), COBOL answers may lag behind as I try to get a handle on the
language again. Note that for the C solutions the input is recieved by piping
it into the executable eg:
	./a.out < input.txt.
However it would be trivial to modify the solutions to open and read the file 
such as is done in the COBOL solutions.

### Code Submissions by Greg Howell
