#include<stdio.h>
#include<stdlib.h>

#define MAX_LEN		10
#define NUM_ELVES	3

typedef struct elf
{
	int cals;
	int pos;
} Elf;

static int elfCompare(const void *a, const void *b)
{
	return (((const struct elf *)a)->cals - ((const struct elf *)b)->cals);
}

void sort (struct elf *arr, const int len)
{
	qsort(arr, len, sizeof(struct elf), elfCompare);
}

int getInput(char *buffer)
{
	int i = 0;
	int c;
	while(i < MAX_LEN - 1 && (c = getchar()) != EOF && c != '\n')
	{
		buffer[i++] = c;
	}
	buffer[i] = '\0';

	return (c == '\n') ? i : -1;
}

int getMaxCalories(struct elf *top_elves)
{
	int i;
	int cur_elf = 1;
	int cur_cal = 0;
	char buffer[MAX_LEN];

	for(;;)
	{
		switch(getInput(buffer))
		{
			case -1:
				return 0;
			case 0:
				for (i = 0; i < NUM_ELVES; i++)
				{
					if (cur_cal > top_elves[i].cals)
					{
						top_elves[i].cals = cur_cal;
						top_elves[i].pos = cur_elf;
						sort(top_elves, NUM_ELVES);
						i = NUM_ELVES;
					}
				}
				cur_cal = 0;
				cur_elf++;
				break;
			default:
				cur_cal += atoi(buffer);
				break;
		}
	}
}

int main(int argc, char **argv)
{
	int i;
	int total = 0;

	Elf max_elves[NUM_ELVES];

	if (getMaxCalories(max_elves) != 0) 
	{
		return 1;
	}
	for (i = 0; i < NUM_ELVES; i++)
	{
		fprintf(stdout, "Elf #%d with %d calories\n", 
			max_elves[i].pos, max_elves[i].cals);
		total += max_elves[i].cals;
	}
	fprintf(stdout, "Total calories: %d\n", total);

	return 0;
}
