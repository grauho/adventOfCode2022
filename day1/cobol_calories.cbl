      *****************************************
      * Author: grauho                        *
      * Date: 20221201                        *
      * Purpose: Holiday Cheer, Day 1         *
      * Tectonics: cobc                       *
      *****************************************

      *---------------------------------------
      * IDENTIFICATION DIVISION
      *---------------------------------------
      * Identification division stores information about the program
      * The only manditory entery is program-ID 
      * Don't forget the periods
       IDENTIFICATION DIVISION.
       PROGRAM-ID. COBOL_CALORIES.
       AUTHOR. GRAUHO.
       DATE-WRITTEN. 20221201. 

      *---------------------------------------
      * ENVIRONMENT DIVISION   
      *---------------------------------------
      * Environment division contains program environment info
      * Split into the configuration and Input-Output sections
      * File-control. lives in Input-Output and allows the user to
      * - specify where on the local computer a file is (PATH) 
       ENVIRONMENT DIVISION.	
        INPUT-OUTPUT SECTION.
                FILE-CONTROL.
                SELECT TESTF ASSIGN TO 'input.txt'
                ORGANIZATION IS LINE SEQUENTIAL.

      *---------------------------------------
      * DATA DIVISION          
      *---------------------------------------
      * Data division contains descriptions of the data used
      * Divided into four sections: file, working-storage, linkage,
      * and report. First two are the most important
      * Linkage only used in subprograms
       DATA DIVISION.
           FILE SECTION.
           FD TESTF.
           01 TEST_FILE PIC 9(10).

      * Working storage describes general variabels in program
       WORKING-STORAGE SECTION.
        01 WS_TEST_FILE PIC 9(10).
        01 WS_EOF       PIC A(01).

        01 ELF_LIM      PIC 9(1)        VALUE 3.

        01 CUR_CAL      PIC 9(10)       VALUE ZEROS.
        01 CUR_ELF      PIC 9(04)       VALUE 1.

        01 S1           PIC 9(04) COMP  VALUE 0.
        01 S2           PIC 9(04) COMP  VALUE 0.
        01 TMP_VAL1     PIC 9(10)       VALUE ZEROS.
        01 TMP_VAL2     PIC 9(04)       VALUE ZEROS.
          
        01 ELF_TAB.
                05 ELF OCCURS 3 TIMES.
                        10 ELF_CAL PIC 9(10) VALUE ZEROS.
                        10 ELF_POS PIC 9(04) VALUE ZEROS.

      *---------------------------------------
      * PROCEDURE DIVISION     
      *---------------------------------------
      * Procedure division contains the algorithms
      * Must contain at least one paragraph,
      * one sentance, and one statement
      * Can be divided into sections
       PROCEDURE DIVISION.
       MAIN.
        OPEN INPUT TESTF.
                PERFORM UNTIL WS_EOF='Y'
                READ TESTF INTO WS_TEST_FILE
                        AT END MOVE 'Y' TO WS_EOF
                        NOT AT END EVALUATE WS_TEST_FILE
                                WHEN 0
                                        PERFORM VARYING S1 FROM 1 BY 1
                                        UNTIL S1 > ELF_LIM
                                                IF (CUR_CAL >
                                                ELF_CAL(S1))
                                                        MOVE CUR_CAL TO
                                                        ELF_CAL(S1)
                                                        MOVE CUR_ELF TO
                                                        ELF_POS(S1)
                                                END-IF
                                                MOVE 0 TO CUR_CAL
                                        END-PERFORM
                                        ADD CUR_ELF 1 GIVING CUR_ELF
                                        PERFORM ELFSORT
                                WHEN OTHER
                                        ADD CUR_CAL WS_TEST_FILE GIVING
                                        CUR_CAL
                        END-EVALUATE
                END-READ
                END-PERFORM
        CLOSE TESTF
        MOVE 0 to CUR_CAL
        PERFORM VARYING S1 FROM 1 BY 1 UNTIL S1 > ELF_LIM
           DISPLAY "ELF " ELF_POS(S1) " CALORIES " ELF_CAL(S1)
           ADD ELF_CAL(S1) CUR_CAL GIVING CUR_CAL
        END-PERFORM.
        DISPLAY "TOTAL CALORIES " CUR_CAL
        STOP RUN.
       ELFSORT. 
        PERFORM VARYING S1 FROM 1 BY 1 UNTIL S1 > ELF_LIM
                PERFORM VARYING S2 FROM S1 BY 1 UNTIL S2 > ELF_LIM
                        IF (ELF_CAL(S2) < ELF_CAL(S1))
                                MOVE ELF_CAL(S1) TO TMP_VAL1
                                MOVE ELF_POS(S1) TO TMP_VAL2

                                MOVE ELF_CAL(S2) TO ELF_CAL(S1)
                                MOVE ELF_POS(S2) TO ELF_POS(S1)

                                MOVE TMP_VAL1    TO ELF_CAL(S2)
                                MOVE TMP_VAL2    TO ELF_POS(S2)
                        END-IF
                END-PERFORM
        END-PERFORM.
