#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int getInput(int *value)
{
	char buffer[12];
	int i = 0; 
	int mode = 0;
	int c;

	while (i < 12 && (c = getchar()) != EOF && c != '\n')
	{
		buffer[i++] = c;
	}
	buffer[i] = '\0';

	if (c == EOF) return -1;

	if (strncmp(buffer, "noop", 4) != 0)
	{
		if (sscanf(buffer + 4, "%d", value) != 1)
		{
			fprintf(stderr, "sscanf error\n");
			exit(1);
		}
		else
		{
			mode = 1;
		}
	}

	return mode;
}

#define SCREEN_SIZE 240
int screen[SCREEN_SIZE] = {0};

void drawPixel(int cycle, int reg_val)
{
	static int i 	= 0;
	int mod_i	= i % 40;

	if ((i < SCREEN_SIZE) 
	&& (mod_i >= reg_val - 1 && mod_i <= reg_val + 1))
	{
		screen[i] = 1;	
	}
	i++;

	return;
}

void printScreen(void)
{
	int i;
	for (i = 0; i < SCREEN_SIZE; i++)
	{
		(screen[i]) ? putc('#', stdout) : putc('.', stdout);
		if ((i + 1) % 40 == 0) putc('\n', stdout);
	}
	return;
}

int testCycles(int cycle, int reg_val)
{
	drawPixel(cycle, reg_val);
	if (!((cycle - 20) % 40))
	{
		fprintf(stdout, "%d: X-reg %d strength = %d\n", 
			cycle, reg_val, cycle * reg_val);
		return (cycle * reg_val);
	}
	return 0;
}

int getCycles(int lim)
{
	int reg_val	= 1;
	int add_val	= 0;
	int mode	= 0;
	int cycle	= 0;
	int total_sum	= 0;

	while (cycle < lim && (mode = getInput(&add_val)) != -1)
	{
		cycle++;
		total_sum += testCycles(cycle, reg_val);

		if (mode)
		{
			cycle++;	
			total_sum += testCycles(cycle, reg_val);
			reg_val += add_val;
		}
	}
	return total_sum;
}

int main(int argc, char **argv)
{
	fprintf(stdout, "Total Sum %d\n\n", getCycles(240));
	printScreen();
	putc('\n', stdout);
	return 0;
}

