#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define BUFF_SIZE 1024
#define MONKEYS 12

typedef struct node
{
	long int val;
	struct node *next;
} Node;

typedef struct monkey
{
	int arg;
	int opp;
	int test_val;
	int tar_true;
	int tar_false;
	int num_inspect;
	struct node *list;
} Monkey;

/* Globals because I'm lazy and awful, you could allocate and reallocate your
 * monkeys and it wouldn't be difficult */
struct monkey monkeys[MONKEYS];
int num_monks	= 0;
int mod_magic 	= 1;

int getInput(char *buffer)
{
	int c;
	int i = 0;
	int flag = 0;

	/* We only care about the things after the colon or arithmetic sign */	
	while (i < BUFF_SIZE && (c = getchar()) != EOF && c != '\n')
	{
		switch(c)
		{
			case '+':
			case '-':
			case '*':
			case '/':
			case '0'...'9':
				flag = 1;
				break;
		}

		if (flag)
		{
			buffer[i++] = c;
		}
	}
	buffer[i] = '\0';

	return (c == EOF) ? -1 : i;
}

struct node *allocNode(int val)
{
	struct node *new = malloc(sizeof(struct node));
	if (!new) exit(1);
	new->val	= val;
	new->next	= NULL;

	return new;
}

struct node* getList(struct monkey m, char *buff)
{
	char num_buff[4];
	int i, j;
	int flag = 0;
	struct node *tmp;

	for (i = 0; buff[i] != '\0'; i++)
	{
		switch(buff[i])	
		{
			case ',':
				num_buff[j] = '\0';
				if (flag == 0)
				{
					m.list = allocNode(atoi(num_buff));
					tmp    = m.list;
					flag = 1;
				}
				else
				{
					tmp->next = allocNode(atoi(num_buff));
					tmp = tmp->next;
				}
				j = 0;
				break;
			case '0'...'9':
				num_buff[j++] = buff[i];
				break;
			default:
				break;
		}
	}
	num_buff[j] = '\0';
	tmp->next = allocNode(atoi(num_buff));

	return m.list;
}

struct node* appendNode(struct monkey *m, struct node *n)
{
	if (m->list == NULL)
	{
		m->list = n;
		n->next = NULL;
		return m->list;
	}

	struct node *tmp = m->list;
	while (tmp->next != NULL)
	{
		tmp = tmp->next;
	}
	tmp->next = n;
	n->next = NULL;
	return m->list;
}

struct node* popNode(struct monkey *m)
{
	struct node *tmp = m->list;
	m->list = m->list->next;
	tmp->next = NULL; 
	return tmp;
}

void printList(struct monkey m)
{
	fprintf(stdout, "Printing list values\n");
	while (m.list != NULL)
	{
		fprintf(stdout, "%ld ", m.list->val);
		m.list = m.list->next;
	}
	fprintf(stdout, "\n\n");
}

int monkeyBusiness()
{
	int i;
	int target = 0;
	long tmp_arg;

	struct node *tmp;

	for (i = 0; i < num_monks; i++)
	{
		while(monkeys[i].list != NULL)
		{
			tmp = popNode(&monkeys[i]);

			/* First inspect the item, ie: change stress based 
			 * on monkeys args */
			monkeys[i].num_inspect++;

			if (monkeys[i].arg == -1)
			{
				tmp_arg = tmp->val;
			}
			else
			{
				tmp_arg = monkeys[i].arg;
			}

			switch(monkeys[i].opp)
			{
				case '+':
					tmp->val += tmp_arg;
					break;
				case '-':
					tmp->val -= tmp_arg;
					break;
				case '*':
					tmp->val *= tmp_arg;
					break;
				case '/':
					tmp->val /= tmp_arg;
					break;
			}

			/* Divide by three round down as a matter of course,
			 * C will already round towards zero by default */
			/* tmp->val /= 3; */

			/* For part 2 we perform a little of the old modulo 
			 * magic with the sum of all the monkey divisors */
			tmp->val %= mod_magic;

			/* Then, test if divisible by the monkeys thing */
			if ((tmp->val % monkeys[i].test_val) == 0)
			{
				target = monkeys[i].tar_true;
			}
			else
			{
				target = monkeys[i].tar_false;
			}

			/* Throw based on result */
			monkeys[target].list 
				= appendNode(&monkeys[target], tmp);
		}
	}

	return 0;
}

long int findMostMonkey(void)
{
	long int first		= 0;
	long int  second	= 0;
	int tmp;
	int i;

	for (i = 0; i < num_monks; i++)
	{
		fprintf(stdout, "Monkey %d inspected %d times\n",
			i, monkeys[i].num_inspect);
		if (monkeys[i].num_inspect > second)
		{
			second = monkeys[i].num_inspect;
		}
		if (second > first)
		{
			tmp = first;
			first = second;
			second = tmp;
		}
	}

	return (first * second);
}

int initMonkeys(void)
{
	char buff[BUFF_SIZE];
	int i = 0;
	int j = 0;
	int mod_i;

	while (getInput(buff) != -1)
	{
		mod_i = i % 7;

		switch (mod_i)
		{
			case 1: 
				monkeys[j].num_inspect = 0;
				monkeys[j].list = 
					getList(monkeys[j], buff);
				break;
			case 2:
				monkeys[j].opp = buff[0];
				if (strcmp(buff + 2, "old") == 0)
				{
					monkeys[j].arg = -1;	
				}
				else
				{
					monkeys[j].arg = atoi(buff + 2);
				}
				break;
			case 3:
				monkeys[j].test_val = atoi(buff);
				mod_magic *= monkeys[j].test_val;
				break;
			case 4:
				monkeys[j].tar_true = atoi(buff);
				break;
			case 5:
				monkeys[j].tar_false = atoi(buff);
				j++;
				break;
			default:
				break;
		}
		i++;
	}

	num_monks = j;
	return 0;
}

void freeNode(struct node *tmp)
{
	if (tmp != NULL)
	{
		freeNode(tmp->next);
		free(tmp);
	}
}

void freeMonkeys(void)
{
	int i;
	for (i = 0; i < num_monks; i++)
	{
		freeNode(monkeys[i].list);
	}
}

int main(int argc, char **argv)
{
	int i;
	initMonkeys();
	for (i = 0; i < 10000; i++)
	{
		monkeyBusiness();
	}
	fprintf(stdout, "Final result %ld\n", findMostMonkey());
	freeMonkeys();

	return 0;
}

