#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct node
{
	int index;
	struct node *next;
	struct node *prev;
	struct node *parent;
} Node;

typedef struct map
{
	int start_i; 
	int end_i;
	int max_x;
	int max_y;
	int size;
	int *arr;
	int *visited;
} Map;

#define ABS_VAL(x) (((x) < 0) ? ((x) * -1) : ((x)))

struct map *initMap(int mode)
{
	int c, i = 0;
	int arr_size	= 10;

	struct map *new = malloc(sizeof(struct map));
	if (!new) exit(1);
	new->start_i	= 0;
	new->end_i	= 0;

	new->arr = malloc(sizeof(int) * arr_size);
	if (new->arr == 0) exit(1);

	while ((c = getchar()) != EOF)
	{
		switch(c)
		{
			case 'a'...'z':
				if (mode)
				{
					new->arr[i++] = ABS_VAL(c - 'z');
				}
				else
				{
					new->arr[i++] = c - 'a';
				}
				break;
			case 'S':
				if (mode)
				{
					new->end_i = i;
					new->arr[i++] = ABS_VAL(c - 'z');
				}
				else
				{
					new->start_i = i;
					new->arr[i++] = 0;
				}
				break;
			case 'E':
				if (mode)
				{
					new->start_i = i;
					new->arr[i++] = 0;
				}
				else
				{
					new->end_i = i;
					new->arr[i++] = 'z' - 'a';
				}
				break;
			case '\n':
				if (new->max_x == 0)
				{
					new->max_x = i;
				}
				(new->max_y)++;
				break;
			default:
				break;
		}

		if (i == arr_size)
		{
			arr_size *= 2;
			new->arr = realloc(new->arr, arr_size * sizeof(int));
			if (new->arr == NULL) exit(1);
		}
	}

	new->visited = calloc(new->max_x * new->max_y, sizeof(int));
	new->size = new->max_x * new->max_y;

	return new;
}

void printArr(struct map *tar, int select)
{
	int i;

	fprintf(stdout, "Printing array\n\n");
	for (i = 0; i < tar->size; i++)
	{
		if (i == tar->start_i)
		{
			fprintf(stdout, "\033[92;32mS\033[39;49m");
		}
		else if (i == tar->end_i)
		{
			fprintf(stdout, "\033[91;31mE\033[39;49m");
		}
		else if (i == select)
		{
			fprintf(stdout, "\033[22;36m@\033[39;49m");
		}
		else
		{
			fprintf(stdout, "%c", tar->arr[i] + 'a');
		}
		if ((i + 1) % tar->max_x == 0)
		{
			putc('\n', stdout);
		}
	}
	fprintf(stdout, "\nMap size: %d\n\n", tar->size);
}

void freeMap(struct map *board)
{
	free(board->arr);
	free(board->visited);
	free(board);
}

struct node *allocNode(int val)
{
	struct node *new = malloc(sizeof(struct node));
	if (new == NULL) exit(1);
	new->next	= NULL;
	new->prev	= NULL;
	new->parent	= NULL;
	new->index	= val;
	return new;
}


struct node* enqueue(struct node *head, struct node *new)
{
	new->next = head;
	return new;
}

struct node* dequeue(struct node *head)
{
	struct node *tmp = head;
	struct node *ret;

	if (head->next == NULL)
	{
		return head;
	}
	while (tmp->next->next != NULL)
	{
		tmp = tmp->next;
	}
	ret = tmp->next;
	tmp->next = NULL;

	return ret;
}

void freeQueue(struct node *head)
{
	if (head != NULL)
	{
		freeQueue(head->next);
		free(head);
	}
}

#define encoord(m, x, y)	((y * m->max_x) + x)
#define decoord_x(m, i)		(i % m->max_x)
#define decoord_y(m, i)		(i / m->max_x)

int *getNeighbors(struct map *board, struct node *curr)
{
	static int neighbors[4] = {-1};
	int curr_x = decoord_x(board, curr->index); 
	int curr_y = decoord_y(board, curr->index);

	memset(neighbors, -1, 4);

	if (curr_x - 1 >= 0)
	{
		neighbors[0] = encoord(board, (curr_x - 1), curr_y);
	}
	if (curr_x + 1 < board->max_x)
	{
		neighbors[1] = encoord(board, (curr_x + 1), curr_y);
	}
	if (curr_y - 1 >= 0)
	{
		neighbors[2] = encoord(board, curr_x, (curr_y - 1));
	}
	if (curr_y + 1 < board->max_y)
	{
		neighbors[3] = encoord(board, curr_x, (curr_y + 1));
	}

	return neighbors;
}

int printStack(struct map *board, struct node *end)
{
	int len = 0;

	fprintf(stdout, "\n\n");
	while (end != NULL)
	{
		len++;
		fprintf(stdout, "%c -> ", board->arr[end->index] + 'a');
		end = end->parent;
	}
	fprintf(stdout, "END\n\n");

	return len;
}

/* The trick is tracking visited vertexes, I just need to figure out how
 * to do that exactly */
int findShortestPath(struct map *board, int mode)
{
	/* If we're tracking vertices there's no need to label the root
	 * as having been explored */
	struct node *queue = NULL;
	struct node *tmp;
	struct node *new;
	int i, ret;
	int *neigh_i;

	queue = enqueue(NULL, allocNode(board->start_i));

	while (queue != NULL)
	{
		tmp = dequeue(queue);	

		/* If tmp has the end index, return */
		if (mode && (board->arr[tmp->index] == ('z' - 'a'))
		|| (!mode && tmp->index == board->end_i))
		{
			ret = printStack(board, tmp) - 1;
			freeQueue(queue);
			return ret;
		}

		/* Else, check all the neighboring vertices to see if they've
		 * been visited */
		neigh_i = getNeighbors(board, tmp);
		for (i = 0; i < 4; i++)
		{
			/* No valid neighbor (likely an edge) 
			 * OR vertex has already been visited
			 * OR vertex is too high up to reach */
			if ((neigh_i[i] == -1)
			|| (board->visited[neigh_i[i]] == 1)
			|| (board->arr[neigh_i[i]] - 1 
				> board->arr[tmp->index]))
			{
				continue;
			}

			/* Label as visited and enqueue */
			board->visited[neigh_i[i]] = 1;
			new = allocNode(neigh_i[i]);
			new->parent = tmp;
			queue = enqueue(queue, new);
		}
	}

	return 0;
}

int main(int argc, char **argv)
{
	int mode = 0;
	struct map *board;

	mode = (argc > 1);
	board = initMap(mode);

	printArr(board, -1);
	fprintf(stdout, "Shortest path %d\n", 
		findShortestPath(board, mode));
	freeMap(board);

	return 0;
}

