#include<stdio.h>
#include<stdlib.h>

#define HASH_SIZE 1024

typedef struct coord
{
	int x;
	int y;
	struct coord *next;
} Coord;

struct coord *hash[HASH_SIZE];
int max_y = 0;

struct coord *allocCoord(int x, int y)
{
	struct coord *new = malloc(sizeof(struct coord));
	if (!new) exit(1);
	new->x		= x;
	new->y		= y;
	new->next	= NULL;

	return new;
}

int hashCoord(struct coord *new)
{
	int key = (new->x * new->y) % HASH_SIZE;
	struct coord *tmp = hash[key];
	if (tmp == NULL)
	{
		hash[key] = new;
	}
	else
	{
		while (1)
		{
			if (tmp->x == new->x && tmp->y == new->y)
			{
				free(new);
				return 1;
			}
			if (tmp->next == NULL)
			{
				break;
			}
			tmp = tmp->next;
		}
		tmp->next = new;
	}

	return 0;
}

int lookupCoord(int x, int y)
{
	int key = (x * y) % HASH_SIZE;
	struct coord *tmp = hash[key];
	if (tmp == NULL)
	{
		return 0;
	}
	else
	{
		while (1)
		{
			if (tmp->x == x && tmp->y == y)
			{
				return 1;
			}
			if (tmp->next == NULL)
			{
				break;
			}
			tmp = tmp->next;
		}
	}
	return 0;
}

#define SWAP(t, x, y) {t tmp = x; x = y; y = tmp;};

void hashRange(int s_x, int s_y, int e_x, int e_y)
{
	int i, j;

	fprintf(stdout, "%dx%d -> %dx%d\n",
		s_x, s_y, e_x, e_y);

	if (s_x > e_x)
	{
		SWAP(int, s_x, e_x);
	}
	if (s_y > e_y)
	{
		SWAP(int, s_y, e_y);
	}

	for (i = s_x; i <= e_x; i++)
	{
		for (j = s_y; j <= e_y; j++)
		{
			hashCoord(allocCoord(i, j));
			fprintf(stdout, "Coord (%d x %d) hashed\n",
				i, j);
		}
	}
}

int initBoard(void)
{
	int c, i = 0, first = 1, x = 0;
	char buff[6];
	int old_x = 0, old_y = 0, new_x = 0, new_y = 0;

	while ((c = getchar()) != EOF)
	{
		switch(c)
		{
			case '0'...'9':
				buff[i++] = c;
				break;
			default:
				buff[i] = '\0';
				if (i > 0)
				{
					if (first == 1)
					{
						switch (x % 4)
						{
						case 0:
							old_x = atoi(buff);
							break;
						case 1:
							old_y = atoi(buff);
							if (old_y > max_y)
							{
								max_y = old_y;
							}
							break;
						case 2:
							new_x = atoi(buff);
							break;
						case 3:
							new_y = atoi(buff);
							if (new_y > max_y)
							{
								max_y = new_y;
							}
							hashRange(old_x, old_y,
								new_x, new_y);
							first = 0;
							break;
						}
					}
					else
					{
						switch (x % 2)
						{
						case 0:
							old_x = new_x;
							new_x = atoi(buff);
							break;
						case 1:
							old_y = new_y;
							new_y = atoi(buff);
							if (new_y > max_y)
							{
								max_y = new_y;
							}
							hashRange(old_x, old_y,
								new_x, new_y);
							break;
						}
					}
					i = 0;
					x++;
				}
				if (c == '\n') 
				{
					first = 1;
					x = 0;
				}
				break;
		}
	}

	return 0;
}

int nextMove(int x, int y, int mode)
{
	for (;;)
	{
		if (mode && y == max_y + 1)
		{
			if (lookupCoord(x, y) == 1)
			{
				return 0;
			}
			else 
			{
				hashCoord(allocCoord(x, y));
				return 1;
			}
		}
		else if (!mode && y > max_y)
		{
			return 0;
		}
		else if (lookupCoord(x, y + 1) == 0)
		{
			y++;
		}
		else if (lookupCoord(x - 1, y + 1) == 0)
		{
			x--; y++;
		}
		else if (lookupCoord(x + 1, y + 1) == 0)
		{
			x++; y++;
		}
		else /* Sand has come to rest, try putting it in */
		{
			if (hashCoord(allocCoord(x, y)) == 1)
			{
				return 0;
			}
			return 1;
		}
	}
}

int simSand(int mode)
{
	const int spawn_x = 500;
	const int spawn_y = 0;
	int rc, score = 0;

	while ((rc = nextMove(spawn_x, spawn_y, mode)) != 0)
	{
		score += rc;	
	}

	return score;
}

static void freeNode(struct coord *tmp)
{
	if (tmp != NULL)
	{
		freeNode(tmp->next);
		free(tmp);
	}
}

void freeBoard(void)
{
	int i;
	for (i = 0; i < HASH_SIZE; i++)
	{
		freeNode(hash[i]);
	}
}

int main(int argc, char **argv)
{
	int mode = 0;
	if (argc > 1) mode = 1;

	initBoard();
	fprintf(stdout, "%d units of sand fall before abyss\n", simSand(mode));
	freeBoard();
	return 0;
}

