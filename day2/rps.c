#include<stdio.h>
#include<stdlib.h>

/* res is just used for the printing */
const char res[]	= {'R', 'P', 'S'}; 
const int loses[] 	= {2, 0, 1};
const int beats[] 	= {1, 2, 0};
int remix = 0;

static int convert(const char in)
{
	int ret;
	switch(in)
	{
		case 'A':
		case 'X':
			ret = 0;
			break;
		case 'B':
		case 'Y':
			ret = 1;
			break;
		case 'C':
		case 'Z':
			ret = 2;
			break;
		default:
			ret = -1;
			break;
	}
	return ret;
}

static int convertRemix(const char in, const int opp_i)
{
	int ret;
	switch(in)
	{
		case 'X': /* LOSE */
			ret = loses[opp_i];
			break;
		case 'Y': /* TIE */
			ret = opp_i;
			break;
		case 'Z': /* WIN */
			ret = beats[opp_i];
			break;
		default:
			ret = -1;
			break;
	}
	return ret;
}

static int getInput(int *opp, int *player)
{
	int c;
	int flag = 0;
	while ((c = getchar()) != EOF && c != '\n')
	{
		if (c != ' ')
		{
			switch(flag)
			{
				case 0:
					*opp = convert(c);
					flag++;
					break;
				case 1:
					if (remix == 0)
					{
						*player = convert(c);
					}
					else
					{
						*player = convertRemix(c,*opp);
					}
					flag++;
					break;
				default:
					return 1;
					break;
			}
		}
	}
	return (c == EOF);
}

int calculateScore(void)
{
	int opp_i, player_i;
	int score = 0;

	while (getInput(&opp_i, &player_i) != 1)
	{
		score		+= player_i + 1;

		if (res[player_i] == res[opp_i])
		{
			fprintf(stdout, "Draw\t%c = %c\n", 
				res[opp_i], res[player_i]);
			score += 3;
		}
		else if (player_i == beats[opp_i])
		{
			fprintf(stdout, "Win\t%c < %c\n", 
				res[opp_i], res[player_i]);
			score += 6;
		}
		else
		{
			fprintf(stdout, "Loss\t%c > %c\n", 
				res[opp_i], res[player_i]);
		}
	}
	return score;
}

int main(int argc, char **argv)
{
	if (argc > 1 && (argv[1][0] == '-'))
	{
		switch (argv[1][1])
		{
			case '1'...'9':
				remix = 1;
				break;
			default:
				break;
				
		}
	}
	fprintf(stdout, "Final score: %d\n", calculateScore());
	return 0;
}

