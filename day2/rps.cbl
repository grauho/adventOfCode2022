      *****************************************
      * Author: grauho                        *
      * Date:   20221202                      *
      * Purpose: Holiday Cheer, Day 2         *
      * Tectonics: cobc                       *
      *****************************************

      *---------------------------------------
      * IDENTIFICATION DIVISION
      *---------------------------------------
      * Identification division stores information about the program
      * The only manditory entery is program-ID 
      * Don't forget the periods
       IDENTIFICATION DIVISION.
       PROGRAM-ID. ROCK-PAPER-SCISSORS.
       AUTHOR. GRAUHO.
       DATE-WRITTEN. 20221202.

      *---------------------------------------
      * ENVIRONMENT DIVISION   
      *---------------------------------------
      * Environment division contains program environment info
      * Split into the configuration and Input-Output sections
      * File-control. lives in Input-Output and allows the user to
      * - specify where on the local computer a file is (PATH) 
       ENVIRONMENT DIVISION.	
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
            SELECT ELF-FILE
                ASSIGN TO "input.txt"
                ORGANIZATION IS LINE SEQUENTIAL.

      *---------------------------------------
      * DATA DIVISION          
      *---------------------------------------
      * Data division contains descriptions of the data used
      * Divided into four sections: file, working-storage, linkage,
      * and report. First two are the most important
      * Linkage only used in subprograms
       DATA DIVISION.
      * Describes data to or from external source
       FILE SECTION.
       FD   ELF-FILE
            LABEL RECORDS ARE STANDARD.
       01   ELF-RECORD.
            05 ELF-RECORD-OPP   PIC X(01) VALUE SPACES.
            05 FILLER           PIC X(01) VALUE " ".
            05 ELF-RECORD-YOU   PIC X(01) VALUE SPACES.

      * Working storage describes general variabels in program
       WORKING-STORAGE SECTION.
       01   WS-RECORD.
            05 WS-RECORD-OPP    PIC X(01) VALUE SPACES.
            05 FILLER           PIC X(01) VALUE " ".
            05 WS-RECORD-YOU    PIC X(01) VALUE SPACES.

       77   END-OF-FILE         PIC X(01) VALUE "N".
            88 AT-END-OF-FILE             VALUE "Y","y".

       77   PLAYER-SCORE        PIC 9(05) VALUE ZEROS.
       77   DISP-SCORE          PIC ZZZZ9 VALUE ZEROS.

       77   PART-NUMBER         PIC 9(01) VALUE ZERO.
            88 IS-VALID-PART              VALUE "1","2".

      *     Conversion tables
       77   WS-GAME-ENCODED     PIC X(06) VALUE "ABCXYZ".
       77   WS-GAME-DECODED     PIC 9(06) VALUE "123123".
       77   WS-GAME-TMP         PIC 9(01).

       77   WS-CONV-YOU         PIC 9(01).
       77   WS-CONV-OPP         PIC 9(01).

       77   WS-GAME-LOSES       PIC 9(03) VALUE "312".
       77   WS-GAME-BEATS       PIC 9(03) VALUE "231".

      *---------------------------------------
      * PROCEDURE DIVISION     
      *---------------------------------------
      * Procedure division contains the algorithms
      * Must contain at least one paragraph,
      * one sentance, and one statement
      * Can be divided into sections
       PROCEDURE DIVISION.
       MAIN.
            PERFORM OPENING-PROCEDURE.
            PERFORM GET-PART-NUMBER.
            DISPLAY " ".
            PERFORM READ-NEXT-RECORD.
            PERFORM DO-ANALYSIS
                UNTIL AT-END-OF-FILE.
            PERFORM CLOSING-PROCEDURE.
            MOVE PLAYER-SCORE TO DISP-SCORE.
            DISPLAY "FINAL SCORE: " DISP-SCORE.
       STOP RUN.

       OPENING-PROCEDURE.
            OPEN INPUT ELF-FILE.

       READ-NEXT-RECORD.
            READ ELF-FILE NEXT RECORD
            AT END
            MOVE "Y" TO END-OF-FILE.

       DO-ANALYSIS.
      *     Move from record scope to working storage
            MOVE ELF-RECORD-OPP TO WS-RECORD-OPP.
            MOVE ELF-RECORD-YOU TO WS-RECORD-YOU.

      *     Convert from XYZ and ABC to a standard 1 2 or 3
      *     because COBOL indexes starting from 1
            INSPECT WS-RECORD-OPP
                CONVERTING  WS-GAME-ENCODED
                TO          WS-GAME-DECODED.
            MOVE WS-RECORD-OPP TO WS-CONV-OPP.

            PERFORM DO-PLAYER-CONVERSION.

      *     Player recieves base score based upon move choice
            ADD WS-CONV-YOU TO PLAYER-SCORE.
            DISPLAY WS-RECORD WITH NO ADVANCING.

      *     This "slicing" is an interesting feature of the language
      *     But here we figure out what could beat our foes choice and
      *     use that to determine if the perscribed move does
            MOVE WS-GAME-BEATS(WS-CONV-OPP:1) TO WS-GAME-TMP.

            IF WS-CONV-YOU EQUAL WS-CONV-OPP
                DISPLAY ": DRAW"
                ADD 3 TO PLAYER-SCORE
            ELSE IF WS-CONV-YOU EQUAL WS-GAME-TMP
                DISPLAY ": WIN"
                ADD 6 TO PLAYER-SCORE
            ELSE
                DISPLAY ": LOSS".

            PERFORM READ-NEXT-RECORD.

       DO-PLAYER-CONVERSION.
            INSPECT WS-RECORD-YOU
               CONVERTING  WS-GAME-ENCODED
               TO          WS-GAME-DECODED
            IF PART-NUMBER EQUAL 2
               EVALUATE WS-RECORD-YOU
      *         LOSE
                WHEN 1
                    MOVE WS-GAME-LOSES(WS-CONV-OPP:1) TO WS-CONV-YOU
      *         TIE
                WHEN 2
                    MOVE WS-CONV-OPP TO WS-CONV-YOU
      *         WIN
                WHEN 3
                    MOVE WS-GAME-BEATS(WS-CONV-OPP:1) TO WS-CONV-YOU
            ELSE
                MOVE WS-RECORD-YOU TO WS-CONV-YOU.
                

       GET-PART-NUMBER.
            DISPLAY "Please enter the problem part (1 or 2): ".
            ACCEPT PART-NUMBER.
            IF NOT IS-VALID-PART
                PERFORM GET-PART-NUMBER.

       CLOSING-PROCEDURE.
            CLOSE ELF-FILE.
            DISPLAY "FILE CLOSED".

