#include<stdio.h>
#include<stdlib.h>
#include<string.h> /* For memset */

#define MAX_LEN 1024
#define ALPHA_LEN ('z' - 'A' + 1)

static int getInput(char *buffer)
{
	int c, i = 0;
	while(i < MAX_LEN - 1 && (c = getchar()) != EOF && c != '\n')
	{
		buffer[i++] = c;
	}
	buffer[i] = '\0';

	return (c == EOF) ? -1 : i;
}

/* If the specified number of lines does not divide cleanly into the total
 * number of lines the final incomplete group will not contribute to the total
 * score because it will not be possible for (alpha[j] == lines) to be true */
int getGroupSum(int lines)
{
	char buffer[MAX_LEN];
	int alpha[ALPHA_LEN];

	/* Offsets because the elf system is backwards from ASCII and does
	 * not account for the characters between upper and lower */
	const int up_off  = 27;
	const int low_off = 'A' - 'a' + 1;

	int len, i, j, k;
	int score = 0;
	i = 0;

	memset(alpha, 0, ALPHA_LEN * sizeof(int));

	while ((len = getInput(buffer)) != -1)
	{
		for (j = 0; j < len; j++)
		{
			k = buffer[j] - 'A';

			if (alpha[k] == i)
			{
				alpha[k]++;
			}
		}
		if (i == lines - 1)
		{
			for (j = 0; j <= ALPHA_LEN; j++)
			{
				if (alpha[j] == lines)
				{
					switch(j + 'A')
					{
						case 'A' ... 'Z':
							score += j + up_off;
							break;
						case 'a' ... 'z':
							score += j + low_off;
							break;
						default:
							exit(1);
							break;
					}
					j = ALPHA_LEN;
				}
			}
			memset(alpha, 0, ALPHA_LEN * sizeof(int));
		}
		(i == lines - 1) ? i = 0 : i++;
	}
	return score;
}

int getPrioritySum(void)
{
	char buffer[MAX_LEN];
	int alpha[ALPHA_LEN];

	int len, halfway, i;
	int score = 0;

	memset(alpha, 0, ALPHA_LEN * sizeof(int));

	while ((len = getInput(buffer)) != -1)
	{
		halfway = len / 2;

		for (i = 0; i < halfway; i++)
		{
			alpha[(buffer[i] - 'A')] = 1;
		}

		for (; i < len; i++)
		{
			if (alpha[(buffer[i] - 'A')] == 1)
			{
				switch(buffer[i]) /* Switch on character */
				{
					case 'a'...'z':
						score += 
							(buffer[i] - 'a' + 1);
						break;
					case 'A'...'Z':
						score += 
							(buffer[i] - 'A' + 27);
						break;
					default:
						exit(1);
						break;
				}
				i = len;
			}
		}
		memset(alpha, 0, ALPHA_LEN * sizeof(int));
	}

	return score;
}

int main(int argc, char **argv)
{
	int lines = 1;
	if (argc > 1)
	{
		lines = atoi(argv[1]);
	}
	if (lines == 1)
	{
		fprintf(stdout, "Total: %d\n", getPrioritySum());
	}
	else
	{
		fprintf(stdout, "Grouped Total: %d\n", getGroupSum(lines)); 
	}

	return 0;
}

