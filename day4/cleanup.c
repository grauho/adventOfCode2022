#include<stdio.h>
#include<stdlib.h>

static int getInput(int *arr)
{
	char tmp[4];
	int c, j = 0, i = 0;
	while (i < 4 && (c = getchar()) != EOF)
	{
		switch(c)
		{
			case '0'...'9':
				tmp[j++] = c;
				break;
			case '\n':
				tmp[j] = '\0';
				arr[i] = atoi(tmp);
				i = 4;
				break;
			default:
				tmp[j] = '\0';
				arr[i++] = atoi(tmp);
				j = 0;
				break;
		}
	}
	return (c == EOF) ? -1 : i;
}

static int isSubsumed(int *arr1, int *arr2)
{
	return (arr1[0] <= arr2[0] && arr1[1] >= arr2[1]);
}

/* Note: Subsumed is a special case of overlapped */
static int isOverlapped(int *arr1, int *arr2)
{
	return (isSubsumed(arr1, arr2) 
		|| (arr1[0] <= arr2[0] && arr1[1] >= arr2[0])
		|| (arr1[1] >= arr2[1] && arr1[0] <= arr2[1]));
}

int getSubsumedRanges(void)
{
	int ret, num = 0;
	int ranges[4];
	while ((ret = getInput(ranges)) != -1 && ret == 4)
	{
		/* One could make isOverlapped try both combinations to avoid
		 * unneeded function calls but it would be more cluttered */
		if (isOverlapped(ranges, ranges + 2) 
		||  isOverlapped(ranges + 2, ranges))
		{
			num++;
		}
	}
	return num;
}

int main(int argc, char **argv)
{
	fprintf(stdout, "There are %d subsumed ranges\n", 
		getSubsumedRanges());
	return 0;
}

