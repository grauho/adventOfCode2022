      *****************************************
      * Author: grauho                        *
      * Date:   20221204                      *
      * Purpose:                              *
      * Tectonics: cobc                       *
      *****************************************

      *---------------------------------------
      * IDENTIFICATION DIVISION
      *---------------------------------------
      * Identification division stores information about the program
      * The only manditory entery is program-ID 
      * Don't forget the periods
       IDENTIFICATION DIVISION.
       PROGRAM-ID. CLEANUP.
       AUTHOR. GRAUHO. 
       DATE-WRITTEN. 20221204.

      *---------------------------------------
      * ENVIRONMENT DIVISION   
      *---------------------------------------
      * Environment division contains program environment info
      * Split into the configuration and Input-Output sections
      * File-control. lives in Input-Output and allows the user to
      * - specify where on the local computer a file is (PATH) 
       ENVIRONMENT DIVISION.	

      * The physical description of the file is a set of statement to
      * identify the physical name ofthe file on the disk drive and how
      * that file is organized
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
            SELECT ELF-FILE
                ASSIGN TO "input.txt"
                ORGANIZATION IS LINE SEQUENTIAL.

      *---------------------------------------
      * DATA DIVISION          
      *---------------------------------------
      * Data division contains descriptions of the data used
      * Divided into four sections: file, working-storage, linkage,
      * and report. First two are the most important
      * Linkage only used in subprograms
       DATA DIVISION.
      * Describes data to or from external source
       FILE SECTION.
       FD   ELF-FILE
            LABEL RECORDS ARE STANDARD.
        01  ELF-RECORD.
            05  ELF-RECORD-INPUT    PIC X(12)   VALUE SPACES.

       WORKING-STORAGE SECTION.
       77   WS-ELF-RECORD-INPUT     PIC X(12)   VALUE SPACES.

       01   FIELDS-TO-DISPLAY.
            05   DISP-FL            PIC Z9      VALUE ZEROS.
            05   FILLER             PIC X(01)   VALUE "-".
            05   DISP-FU            PIC Z9      VALUE ZEROS.
            05   FILLER             PIC X(03)   VALUE " : ".
            05   DISP-SL            PIC Z9      VALUE ZEROS.
            05   FILLER             PIC X(01)   VALUE "-".
            05   DISP-SU            PIC Z9      VALUE ZEROS.

       77   NUMBER-SUBSUMED         PIC 9(6)    VALUE 0.
       77   NUMBER-OVERLAPPED       PIC 9(6)    VALUE 0.

       77   END-OF-FILE             PIC X       VALUE "N".
            88 AT-END-OF-FILE                   VALUE "Y","y".

      *---------------------------------------
      * PROCEDURE DIVISION     
      *---------------------------------------
      * Procedure division contains the algorithms
      * Must contain at least one paragraph,
      * one sentance, and one statement
      * Can be divided into sections
       PROCEDURE DIVISION.
       MAIN.
            PERFORM OPENING-PROCEDURE.
            PERFORM READ-NEXT-RECORD.
            PERFORM DISPLAY-RECORDS 
                UNTIL AT-END-OF-FILE.
            PERFORM CLOSING-PROCEDURE.
       STOP RUN.

       DISPLAY-RECORDS.
            PERFORM DISPLAY-FIELDS.
            PERFORM READ-NEXT-RECORD.

       DISPLAY-FIELDS.
            MOVE ELF-RECORD-INPUT TO WS-ELF-RECORD-INPUT.
      *      INSPECT WS-ELF-RECORD-INPUT
      *          REPLACING ALL 
      *          "," BY "-".
            UNSTRING WS-ELF-RECORD-INPUT
                DELIMITED BY "-" OR ","
                INTO    DISP-FL
                        DISP-FU
                        DISP-SL
                        DISP-SU.
            PERFORM TEST-IF-SUBSUMED.
            DISPLAY FIELDS-TO-DISPLAY.

       TEST-IF-SUBSUMED.
            IF (DISP-FL <= DISP-SL AND DISP-FU >= DISP-SU)
            OR (DISP-SL <= DISP-FL AND DISP-SU >= DISP-FU)
                ADD 1 TO NUMBER-SUBSUMED GIVING NUMBER-SUBSUMED
                ADD 1 TO NUMBER-OVERLAPPED GIVING NUMBER-OVERLAPPED
            ELSE
                PERFORM TEST-IF-OVERLAPPED.

       TEST-IF-OVERLAPPED.
            IF (DISP-FL <= DISP-SL AND DISP-FU >= DISP-SL)
            OR (DISP-FU >= DISP-SU AND DISP-FL <= DISP-SU)
            OR (DISP-SL <= DISP-FL AND DISP-SL >= DISP-FU)
            OR (DISP-SU >= DISP-FU AND DISP-SU <= DISP-FL)
                ADD 1 TO NUMBER-OVERLAPPED GIVING NUMBER-OVERLAPPED.
       
       OPENING-PROCEDURE.  
            OPEN INPUT ELF-FILE.

       READ-NEXT-RECORD.
            READ ELF-FILE NEXT RECORD
                AT END
                MOVE "Y" TO END-OF-FILE.

       CLOSING-PROCEDURE.
            CLOSE ELF-FILE.
            DISPLAY "Total number of subsuming occurances:   "
                NUMBER-SUBSUMED.
            DISPLAY "Total number of overlapping occurances: "
                NUMBER-OVERLAPPED.
