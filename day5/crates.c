#include<stdio.h>
#include<stdlib.h>

#define NUM_TOWERS 9
#define MAX_LEN 1024

typedef struct Crate
{
	int label;
	struct Crate *next;
} crate; 

struct Crate *arr[NUM_TOWERS] = {0};

static int getInput(char *buffer)
{
	int c, i = 0;
	while (i < MAX_LEN - 1 && (c = getchar()) != EOF && c != '\n')
	{
		buffer[i++] = c;	
	}
	buffer[i] = '\0';

	return (c == EOF) ? -1 : i;
}


struct Crate *newCrate(int label)
{
	crate *new = malloc(sizeof(struct Crate));
	if (new == NULL)
	{
		fprintf(stderr, "Memory allocation error\n");
	}
	new->label	= label;
	new->next	= NULL;
	return new;
}

struct Crate* pushCrate(struct Crate *new, struct Crate *stack)
{
	new->next = stack;
	return new;
}

struct Crate* popCrate(struct Crate **stack)
{
	struct Crate *tmp = *stack;
	if (tmp != NULL)
	{
		(*stack) = (*stack)->next;
	}
	return tmp;
}

int freeCrates(void)
{
	int i;
	struct Crate *tmp = NULL;
	for (i = 0; i < NUM_TOWERS; i++)
	{
		while(arr[i] != NULL)
		{
			tmp = arr[i];
			arr[i] = tmp->next;
			free(tmp);
		}
	}
	return 0;
}

void initTower(int pos, char *key)
{
	int i;
	for (i = 0; key[i] != '\0'; i++)
	{
		if ((arr[pos] = pushCrate(newCrate(key[i]), arr[pos])) == NULL)
		{
			fprintf(stderr, 
				"Error pushing crate %c onto stack %d\n",
				key[i], pos);
		}
		else
		{
			fprintf(stdout, "Crate %c pushed\n", arr[pos]->label);
		}
	}
}

static void flipTower(struct Crate **tower)
{
	struct Crate *new = NULL;
	struct Crate *tmp = *tower;
	while (tmp != NULL)
	{
		new = pushCrate(popCrate(&tmp), new);
	}
	*tower = new;
}

static void spawnTowers(void)
{
	int i, pos = 0;
	char buff[MAX_LEN];

	while (getInput(buff) != 0)
	{
		for (i = 0; buff[i] != '\0'; i++)
		{
			pos = i / 4;
			switch(buff[i])
			{
				case 'A'...'Z':
					if ((arr[pos] = 
						pushCrate(newCrate(buff[i]), 
							arr[pos])) == NULL)
					{
						fprintf(stderr, 
							"Error pushing\n");
						exit(1);
					}
					else
					{
						fprintf(stdout, "Pushed %c"
							" to position %d\n",
							buff[i], pos);
					}
					break;
				default:
					break;
			}
		}
	}

	for (i = 0; i < NUM_TOWERS; i++)
	{
		if (arr[i] != NULL)
		{
			fprintf(stdout, "Flipping tower %d\n", i);
			flipTower(&arr[i]);
		}
	}
	return;
}

static void doMoves(int mode)
{
	char buff[MAX_LEN];
	int i, times, from, to;
	struct Crate *tmp = NULL;

	while (getInput(buff) != -1)
	{
		if (sscanf(buff, "move %d from %d to %d", &times, &from, &to) 
			!= 3)
		{
			fprintf(stderr, "sscanf error\n");
			exit(1);
		}
		fprintf(stdout, "Moving %d items from %d to %d\n", 
			times, from - 1, to - 1);
		if (mode == 0)
		{
			for (i = 0; i < times; i++)
			{
				arr[to-1] = 
				pushCrate(popCrate(&arr[from-1]), arr[to-1]);
			}
		}
		else
		{
			for (i = 0; i < times; i++)
			{
				tmp = pushCrate(popCrate(&arr[from-1]), tmp);
			}
			while (tmp != NULL)
			{
				arr[to-1] = 
					pushCrate(popCrate(&tmp), arr[to-1]);
			}
		}
	}
}

int main(int argc, char **argv)
{
	int i = 0;

	spawnTowers();
	doMoves(1);

	for (i = 0; i < NUM_TOWERS; i++)
	{
		if (arr[i] != NULL)
		{
			fprintf(stdout, "%d:\ttop:%c\n", i, arr[i]->label);
		}
	}
	freeCrates();
	return 0;
}

