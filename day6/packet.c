#include<stdio.h>
#include<stdlib.h>

#define MAX_LEN 4096

int isMatch(char *buffer, int len)
{
	int i, j;
	int ret = 0;
	for (i = 0; i < len; i++)
	{
		j = i - 1;
		while (j >= 0)
		{
			if (buffer[i] == buffer[j])
			{
				ret = 1;
				i = len;
			}
			j--;
		}
	}
	return ret;
}

int findPacket(int len)
{
	int c, i = 0;
	char master_buffer[MAX_LEN];

	/* As we read in our input we test it */
	for (i = 0; (i < MAX_LEN - 1) && ((c = getchar()) != EOF); i++)
	{
		master_buffer[i] = c;
		if (i >= len)
		{
			/* A little substring trick */
			if (isMatch(master_buffer + i - len, len) == 0)
			{
				return i;
			}
		}
	}
	master_buffer[i] = '\0';
	return -1;
}

int main(int argc, char **argv)
{
	fprintf(stdout, "The start of packet occurs %d characters in\n",
		findPacket(4));
	return 0;
}

