#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAX_LEN 1024

typedef struct node
{
	struct node *parent;
	struct node *left;
	struct node *right;
	long int value;
	char name[64];
} Node;

int getLine(char *buffer)
{
	int c, i = 0;
	for (i = 0; i < MAX_LEN - 1 && (c = getchar()) != EOF && c != '\n';)
	{
		buffer[i++] = c;
	}
	buffer[i] = '\0';
	return (c == EOF) ? -1 : i;
}

static int isRightChild(struct node *node)
{
	return (node == node->parent->right);
}

struct node *newNode(struct node *parent, char *name)
{
	struct node *new = malloc(sizeof(struct node));
	if (new == NULL) exit(1);
	new->parent	= parent;
	new->left	= NULL;
	new->right	= NULL;
	new->value	= 0;
	strncpy(new->name, name, 63);	
	return new;
}

void insertNodeLevel(struct node *parent, char *name)
{
	struct node *tmp = parent;
	if (parent->right == NULL)
	{
		parent->right = newNode(parent, name);
	}
	else
	{
		tmp = parent->right;
		while (tmp->left != NULL)
		{
			tmp = tmp->left;
		}
		tmp->left = newNode(tmp, name);
	}
}

long int findDelCandidate(struct node *tmp, long int fs_size)
{
	static long int best_candidate = 70000000;
	long int target = ((30000000) - (70000000 - fs_size));

	if (tmp != NULL)
	{
		best_candidate = findDelCandidate(tmp->right, fs_size);
		best_candidate = findDelCandidate(tmp->left, fs_size);
		if (tmp->value > target && tmp->value < best_candidate)
		{
			best_candidate = tmp->value;
		}
	}
	return best_candidate;
}

long int doPart1Calculation(struct node *tmp)
{
	long int score = 0;

	if (tmp != NULL)
	{
		score += doPart1Calculation(tmp->right);
		score += doPart1Calculation(tmp->left);
		if (tmp->value < 100000)
		{
			score += tmp->value;
		}
	}
	return score;
}

void getInclusiveSizes(struct node *tmp)
{
	static long int running_score = 0;
	if (tmp != NULL)
	{
		getInclusiveSizes(tmp->right);
		getInclusiveSizes(tmp->left);

		fprintf(stdout, "\n\n--node %s %s child of parent %s--\n", 
			tmp->name, 
			(tmp->parent == NULL) ? "NULL" : 
				(isRightChild(tmp) ? "RIGHT" : "LEFT"),
			(tmp->parent == NULL) ? "NULL" : tmp->parent->name);

		running_score += tmp->value;
		fprintf(stdout, "child %s contributes %ld to running score\n",
			tmp->name, running_score);

		if (tmp->parent != NULL && isRightChild(tmp))
		{
			fprintf(stdout, 
				"child %s contributes %ld to parent %s\n",
				tmp->name, running_score, tmp->parent->name);
			tmp->parent->value += running_score;
			running_score = 0;
		}
		if (tmp->parent != NULL && isRightChild(tmp))
		{
			fprintf(stdout, "parent %s has a size of\t%ld\n\n", 
				tmp->parent->name, 
				tmp->parent->value);
		}
	}
}

void freeTree(struct node *tmp)
{
	if (tmp != NULL)
	{
		freeTree(tmp->right);
		freeTree(tmp->left);
		free(tmp);
	}
}

/* I will admit that this bit is a little hacky */
int process(char *buffer)
{
	int i, ret;
	if (sscanf(buffer, "$ cd %s", buffer) == 1)
	{
		if (strcmp(buffer, "..") == 0)
		{
			ret = 0;
		}
		else
		{
			ret = 1;
		}
		fprintf(stdout, "changing directory %s\n", buffer);
	}
	else if (strcmp(buffer, "$ ls") == 0)
	{
		ret = 2;
	}
	else if (sscanf(buffer, "dir %s", buffer) == 1)
	{
		ret = 3;	
		fprintf(stdout, "new directory %s\n", buffer);
	}
	else
	{
		for (i = 0; buffer[i] != ' '; i++);
		buffer[i] = '\0';
		ret = 4;
		fprintf(stdout, "item size %s\n", buffer);
	}
	return ret;
}

int main(int argc, char **argv)
{
	/* I'm just going to explicitly define our root */
	struct node *root = newNode(NULL, "/");
	struct node *cursor = root;
	char buffer[MAX_LEN];
	long int local_size = 0;

	/* Skip that $ cd / line */
	getLine(buffer);

	while (getLine(buffer) != -1)
	{
		switch(process(buffer))
		{
			case 0: /* cd parent dir */
				cursor->value += local_size;
				fprintf(stdout, "%ld assigned to node %s\n\n",
					local_size, cursor->name);
				local_size = 0;

				while (isRightChild(cursor) == 0)
				{
					cursor = cursor->parent;
				}
				cursor = cursor->parent;
				break;
			case 1: /* cd into a new dir */
				cursor->value += local_size;
				fprintf(stdout, "%ld assigned to node %s\n\n",
					local_size, cursor->name);
				local_size = 0;

				cursor = cursor->right;
				while(strcmp(cursor->name, buffer) != 0)
				{
					if (cursor->left == NULL)
					{
						insertNodeLevel(cursor->parent, 
								buffer);
					}
					cursor = cursor->left;
				}
				break;
			case 2: /* ls, just ignore */
				fprintf(stdout, "ls\n");
				break;
			case 3:	/* dir create new directory */
				insertNodeLevel(cursor, buffer);
				break;
			case 4:
				/* item, tally size */
				local_size += atol(buffer);
				break;
		}
	}
	cursor->value += local_size;
	fprintf(stdout, "%ld assigned to node %s\n\n",
		local_size, cursor->name);

	getInclusiveSizes(root);

	fprintf(stdout, "\n\n");
	fprintf(stdout, "Size of Entire Filesystem\t%ld\n", 
			root->value);
	fprintf(stdout, "Part 1 Modified System Size\t%ld\n", 
			doPart1Calculation(root));
	fprintf(stdout, "Part 2 Deletion Contender\t%ld\n", 
			findDelCandidate(root, root->value));

	freeTree(root);

	return 0;
}

