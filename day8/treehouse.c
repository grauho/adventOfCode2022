#include<stdio.h>
#include<stdlib.h>

typedef struct forest
{
	int width;
	int height;
	int *trees;
} Forest;

#define getTree(f, x, y) (f->trees[(f->width * y) + x])

struct forest* initForest(void)
{
	int c, i = 0;
	int max_size = 1024; 

	struct forest *new = malloc(sizeof(struct forest));
	if (new == NULL)
	{
		exit(1);
	}
	new->trees = NULL;
	new->trees = malloc(sizeof(int) * max_size);
	if (new->trees == NULL) 
	{
		exit(1);
	}

	new->width = 0;
	new->height = 0;

	while ((c = getchar()) != EOF)
	{
		switch(c)
		{
			case '0'...'9':
				/* Reallocate if needed */
				if (i > max_size)
				{
					max_size *= 1.5;
					new->trees = realloc(new->trees,
						sizeof(int) * max_size);
					fprintf(stdout, "reallocating: %d\n", 
						max_size);
				}
				new->trees[i++] = c - '0';
				break;
			case '\n':
				if (new->width == 0)
				{
					new->width = i;
				}
				(new->height)++;
				break;
			default:
				fprintf(stderr, "undefined char %c\n", c);
				exit(1);
				break;
		}
	}

	fprintf(stdout, "%d x %d : %d Trees\n", new->width, new->height, i);

	return new;
}

int isVisible(struct forest *woods, int tree_y, int tree_x)
{

	int i;
	int height = getTree(woods, tree_x, tree_y);

	if (tree_y == 0 || tree_x == 0 
	|| tree_y == woods->height - 1 || tree_x == woods->width - 1)
	{
		return 1;
	}

	for (i = tree_x - 1; i >= 0; i--)
	{
		if (getTree(woods, i, tree_y) >= height)
		{
			break;
		}
		if (i == 0)
		{
			return 1;
		}
	}
	for (i = tree_x + 1; i < woods->width; i++)
	{
		if (getTree(woods, i, tree_y) >= height)
		{
			break;
		}
		if (i == woods->width - 1)
		{
			return 1;
		}
	}
	for (i = tree_y - 1; i >= 0; i--)
	{
		if (getTree(woods, tree_x, i) >= height)
		{
			break;
		}
		if (i == 0)
		{
			return 1;
		}
	}
	for (i = tree_y + 1; i < woods->height; i++)
	{
		if (getTree(woods, tree_x, i) >= height)
		{
			break;
		}
		if (i == woods->height - 1)
		{
			return 1;
		}
	}
	return 0;
}

int isScenic(struct forest *woods, int tree_y, int tree_x)
{
	int i, local_score = 0;
	int total_score = 1;
	int height = getTree(woods, tree_x, tree_y);

	for (i = tree_x - 1; i >= 0; i--)
	{
		local_score++;
		if (getTree(woods, i, tree_y) >= height)
		{
			break;
		}
	}
	total_score *= local_score;
	local_score = 0;

	for (i = tree_x + 1; i < woods->width; i++)
	{
		local_score++;
		if (getTree(woods, i, tree_y) >= height)
		{
			break;
		}
	}
	total_score *= local_score;
	local_score = 0;

	for (i = tree_y - 1; i >= 0; i--)
	{
		local_score++;
		if (getTree(woods, tree_x, i) >= height)
		{
			break;
		}
	}
	total_score *= local_score;
	local_score = 0;

	for (i = tree_y + 1; i < woods->height; i++)
	{
		local_score++;
		if (getTree(woods, tree_x, i) >= height)
		{
			break;
		}
	}
	total_score *= local_score;
	local_score = 0;

	return total_score;
}

int altIsScenic(struct forest *woods, int tree_y, int tree_x)
{
	int i, local_score = 0, total_score = 1;
	int height = getTree(woods, tree_x, tree_y);

	for (i = 0, local_score = 0; i < woods->width; i++)
	{
		if (i == tree_x)
		{
			total_score *= local_score;
			local_score = 0;
		}
		else if (getTree(woods, i, tree_y) >= height)
		{
			if (i > tree_x)
			{
				local_score++;
				break;
			}
			local_score = 1;
		}
		else
		{
			local_score++;
		}
	}
	total_score *= local_score;

	for (i = 0, local_score = 0; i < woods->height; i++)
	{
		if (i == tree_y)
		{
			total_score *= local_score;
			local_score = 0;
		}
		else if (getTree(woods, tree_x, i) >= height)
		{
			if (i > tree_y)
			{
				local_score++;
				break;
			}
			local_score = 1;
		}
		else
		{
			local_score++;
		}
	}
	total_score *= local_score;

	return total_score;
}

void assessForest(struct forest *woods)
{
	int i, j;
	int count	= 0;
	int tmp 	= 0;
	int most_scenic	= 0;

	for (i = 0; i < woods->height; i++)
	{
		for (j = 0; j < woods->width; j++)
		{
			count += isVisible(woods, i, j);
			tmp = isScenic(woods, i, j);
			/* tmp = altIsScenic(woods, i, j); */
			if (tmp > most_scenic)
			{
				most_scenic = tmp;
			}
		}
	}

	fprintf(stdout, "There are %d visible trees\n", count);
	fprintf(stdout, "Most scenic %d\n", most_scenic);
	return;
}

void printForest(struct forest *woods)
{
	int i, j;

	fprintf(stdout, "\n");
	for (i = 0; i < woods->height; i++)
	{
		for (j = 0; j < woods->width; j++)
		{
			fprintf(stderr, "%d ", getTree(woods, j, i));
		}
		fprintf(stdout, "\n");
	}
	fprintf(stdout, "\n");
}

int main(int argc, char **argv)
{
	Forest *woods = initForest();
	/* printForest(woods); */
	assessForest(woods);

	free(woods->trees);
	free(woods);

	return 0;
}

