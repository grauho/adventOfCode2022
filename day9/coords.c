#include<stdio.h>
#include<stdlib.h>

#define TABLE_SIZE 124
#define ABS_VAL(x)	((x < 0) ? (x * -1) : (x))

typedef struct coord
{
	int x;
	int y;
	struct coord *next;
} Coord;

struct coord *hash[TABLE_SIZE];

static int getInput(char *dir, int *moves)
{
	int i = 0;
	int c;
	char buffer[8];

	while (i < 8 && (c = getchar()) != EOF && c != '\n')
	{
		buffer[i++] = c;
	}
	buffer[i] = '\0';

	if (c != EOF && sscanf(buffer, "%c %d", dir, moves) != 2)
	{
		fprintf(stdout, "sscanf failed\n");
	}

	return (c == EOF) ? -1 : 0;
}

static struct coord *allocCoord(int x, int y)
{
	struct coord *new = malloc(sizeof(struct coord));
	if (!new) exit(1);
	new->x		= x;
	new->y		= y;
	new->next	= NULL;
	return new;
}

static void hashCoord(int x, int y)
{
	int key = 0;
	struct coord *tmp;
	struct coord *new = allocCoord(x, y);

	key += x;
	key += y;
	key = ABS_VAL(key);

	tmp = hash[key % TABLE_SIZE];
	if (tmp == NULL)
	{
		hash[key % TABLE_SIZE] = new;
	}
	else
	{
		for(;;)
		{
			if ((tmp->x == x) && (tmp->y == y))
			{
				free(new);
				break;
			}

			if (tmp->next != NULL)
			{
				tmp = tmp->next;
			}
			else
			{
				tmp->next = new;
				break;
			}
		}
	}
}

static void freeCoord(struct coord *tmp)
{
	if (tmp != NULL)
	{
		freeCoord(tmp->next);
		free(tmp);
	}
}

static void freeHash(void)
{
	int i;
	for (i = 0; i < TABLE_SIZE; i++)
	{
		freeCoord(hash[i]);	
	}
}

static int countEntries(void)
{
	int i;
	int count = 0;
	struct coord *tmp;

	for (i = 0; i < TABLE_SIZE; i++)
	{
		tmp = hash[i];
		while (tmp != NULL)
		{
			count++;
			tmp = tmp->next;
		}
	}
	return count;
}

static int isAdjacent(struct coord *head, struct coord *tail)
{
	int i, j;
	for (i = -1; i < 2; i++)
	{
		for (j = -1; j < 2; j++)
		{
			if (tail->x + j == head->x && tail->y + i == head->y)
			{
				return 1;
			}
		}
	}

	return 0;
}

static int isCardinal(struct coord *head, struct coord *tail)
{
	if (head->x == tail->x + 2)
	{
		(tail->x)++;	
		return 1;
	}
	else if (head->x == tail->x - 2)
	{
		(tail->x)--;	
		return 1;
	}
	else if (head->y == tail->y + 2)
	{
		(tail->y)++;	
		return 1;
	}
	else if (head->y == tail->y - 2)
	{
		(tail->y)--;	
		return 1;
	}
	else
	{
		return 0;
	}
}

static int findDiagonal(struct coord *head, struct coord *tail)
{
	struct coord dummy;

	dummy.x = tail->x + 1;
	dummy.y = tail->y + 1;
	if (isAdjacent(head, &dummy))
	{
		tail->x = dummy.x;
		tail->y = dummy.y;
		return 1;
	}
	dummy.x = tail->x + 1;
	dummy.y = tail->y - 1;
	if (isAdjacent(head, &dummy))
	{
		tail->x = dummy.x;
		tail->y = dummy.y;
		return 1;
	}
	dummy.x = tail->x - 1;
	dummy.y = tail->y + 1;
	if (isAdjacent(head, &dummy))
	{
		tail->x = dummy.x;
		tail->y = dummy.y;
		return 1;
	}
	dummy.x = tail->x - 1;
	dummy.y = tail->y - 1;
	if (isAdjacent(head, &dummy))
	{
		tail->x = dummy.x;
		tail->y = dummy.y;
		return 1;
	}
	return 0;
}

static int isTail(struct coord *node)
{
	return (node->next == NULL);
}

int getNumSquares(int length)
{
	char dir;
	int times, i;

	/* Make sure to put in origin */
	hashCoord(0, 0);

	struct coord *head = allocCoord(0, 0);
	struct coord *tmp = head;

	for (i = 1; i < length; i++, tmp = tmp->next)
	{
		tmp->next = allocCoord(0, 0);
	}

	while (getInput(&dir, &times) != -1)
	{
		for (i = 0; i < times; i++)
		{
			tmp = head;

			switch(dir)
			{
				case 'R':
					(head->x)++;
					break;
				case 'L':
					(head->x)--;
					break;
				case 'U':
					(head->y)++;
					break;
				case 'D':
					(head->y)--;
					break;
			}

			while (tmp->next != NULL)
			{
				/* No need to move! */
				if (isAdjacent(tmp, tmp->next) == 0)
				{
					/* Check if in a different row or 
					 * column, if so daigonal */
					if(tmp->x != tmp->next->x
					&& tmp->y != tmp->next->y)
					{
						findDiagonal(tmp, tmp->next);	
					}
					else
					{	
						if (!isCardinal(tmp, 
								tmp->next))
						{
							exit(1);
						}
					}

					if (isTail(tmp->next))
					{
						hashCoord(tmp->next->x, 
								tmp->next->y);
					}
				}
				tmp = tmp->next;
			}
		}
	}
	freeCoord(head);

	return countEntries();
}

int main(int argc, char **argv)
{
	int len = 2;

	/* No guardrails on this one */
	if (argc > 1)
	{
		len = atoi(argv[1]);
	}
	fprintf(stdout, "Number of hashed coordinates %d\n", 
			getNumSquares(len));
	fprintf(stdout, "Program Terminated Successfully!\n");
	freeHash();
	return 0;
}

